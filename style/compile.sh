#!/bin/bash

dir=$(cd "$(dirname ${BASH_SOURCE[0]})" &>/dev/null && pwd)
input="${dir}/style.scss"
output="${dir}/style.css"

function compile()
{
	sassc -mt compact "${input}" > "${output}"
}

compile

while true; do
	inotifywait -e modify "${input}"
	compile
done
