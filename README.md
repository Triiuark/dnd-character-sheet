# D&D 5th Edition Character Sheet
* standalone web page - no internet required
* open the included index.html in your (modern) browser
* press &lt;CTRL&gt;+&lt;Enter&gt; to go to the next input field (on multi line inputs)
* click on "More" to find more settings - yeah!
* "Extra Rows" is the number of empty rows in lists like "Equipment"
* reload the page to get new empty rows and update ability score modifiers, saving throws and skills
* "Page Width" and "Page Height" are in mm
* only checked pages will be printed
* sortable "boxes" and rows - realized with [html5sortable](https://github.com/lukasoppermann/html5sortable) (maintained by Lukas Oppermann, published under the MIT License (MIT))
* if you encounter problems when editing try to disable sorting
* use your browser zoom feature to move boxes or if you can't read shit
* click the download button to save your character
* print with background and background images enabled
* if you want to see an example: open the included Example-Character.json with the file input (upload) button
* (open any file to reset all data)