"use strict"

class DnDCharacter
{
	constructor()
	{
		this.idCounter = 0;

		window.addEventListener('beforeunload', this.unloadListener);

		try
		{
			this.data = JSON.parse(window.localStorage.getItem('data'));
		}
		catch (ex)
		{
		}
		if (!this.data || typeof this.data != 'object' || typeof this.data.character != 'object' || typeof this.data.settings != 'object')
		{
			this.data = {settings: {}, character: {}};
		}

		let download      = this.createNode({tagName: 'a', id: 'download', children: [ 'Download' ]});
		let upload        = this.createNode({tagName: 'input', id: 'upload', type: 'file'});
		let exportText    = this.createNode({tagName: 'textarea', rows: 10, cols: 20, id: 'export'});
		let exportButton  = this.createNode({tagName: 'button', children: ['Show JSON']});
		let headerButton  = this.createNode({tagName: 'button', children: ['More']});
		let sortingButton = this.createNode({tagName: 'button', children: ['Enable Sorting']});
		let pages         = this.createNode({tagName: 'div'});
		let extraHeader   = this.createNode({tagName: 'div', class: 'extra'});
		let header        = this.createNode({tagName: 'header', children: [download, upload, sortingButton, exportButton, headerButton, extraHeader]});

		this.holderColumn = this.createNode({tagName: 'div', id: 'holderColumn', class: 'column'});

		[['Border Color', 'black'], ['Extra Rows', 5], ['Page Width', 183], ['Page Height', 268]].forEach(it =>
		{
			let key     = this.camelCase(it[0]);
			let type    = 'color';
			if (it[0] != 'Border Color')
			{
				type = 'number';
				this[key] = isNaN(this.data.settings[key]) || typeof this.data.settings[key] != 'string' || this.data.settings[key].length < 1 ? it[1] : this.data.settings[key];
			}
			else
			{
				this[key] = this.data.settings[key] ? this.data.settings[key] : it[1];
			}

			let element = this.createNode({tagName: 'label', children: [ {tagName: 'span', children: [it[0] + ': ']}, {tagName: 'input', type: type, value: this[key]} ]});
			element.lastChild.addEventListener('change', ev =>
			{
				this.data.settings[key] = ev.target.value;
				location.reload();
			});
			extraHeader.appendChild(element);
		});

		extraHeader.appendChild(pages);

		exportButton.addEventListener('click', ev => { exportText.style.display = exportText.style.display != 'block' ? 'block' : 'none'; ev.target.innerText = exportText.style.display == 'block' ? 'Hide JSON' : 'Show JSON' });
		download.addEventListener('click', this.download.bind(this));
		upload.addEventListener('change', this.upload.bind(this));
		sortingButton.addEventListener('click', this.toggleSorting.bind(this, sortingButton));
		headerButton.addEventListener('click', this.toggleHeader.bind(this, headerButton));
		//exportText.addEventListener('change', this.editJson.bind(this, exportText));

		document.body.appendChild(header);
		document.body.appendChild(exportText);
		document.body.appendChild(this.holderColumn);

		this.createSheet();
		this.setPositions();

		if (!Array.isArray(this.data.settings.disabledPages))
		{
			this.data.settings.disabledPages = [];
		}
		let pageElements = document.querySelectorAll('div.page');
		for (var ii = 0; ii < pageElements.length; ++ii)
		{
			let checkbox = this.createNode({ tagName: 'input', type: 'checkbox', value: ii });
			if (this.data.settings.disabledPages.indexOf('' + ii) < 0) {
				checkbox.checked = true;
			}
			else
			{
				pageElements[ii].classList.add('noPrint');
			}
			pages.appendChild(this.createNode({tagName: 'label', children: ['Page ' + (ii + 1), checkbox]}));


			checkbox.addEventListener('change', ev =>
			{
				let index = this.data.settings.disabledPages.indexOf(ev.target.value);
				if (ev.target.checked && index > -1)
				{
					this.data.settings.disabledPages.splice(index, 1);
					location.reload();
				}
				else if(!ev.target.checked && index < 0)
				{
					this.data.settings.disabledPages.push(ev.target.value);
					location.reload();
				}
			});
		}

		document.querySelectorAll('input, select, textarea').forEach(it => it.addEventListener('change', ev => this.setChanged));
	};

	unloadListener(ev)
	{
		let message = "Are you sure?";
		ev.preventDefault();
		ev.returnValue = message;
		return message;
	}

	setChanged()
	{
		console.log('now');
		if (this.changed !== true)
		{
			window.addEventListener('beforeunload', ev => alert('Unload'));
		}
		this.changed = true;
	}

	/*
	editJson(element)
	{
		try
		{
			let obj = JSON.parse(element.value.trim());
			this.setData(obj);
		}
		catch(ex)
		{
			console.log(ex);
		}
	}
	*/

	toggleHeader(element)
	{
		let extra = document.querySelector('header > div.extra');
		if (extra.style.display != 'grid')
		{
			extra.style.display = 'grid';
			element.textContent = 'Less';
		}
		else
		{
			extra.style.display = 'none';
			element.textContent = 'More';
		}
	}

	toggleSorting(element)
	{
		if(typeof sortable != 'function')
		{
			element.textContent = 'Error';
			return;
		}
		if (this.sortingEnabled)
		{
			sortable('div.column', 'disable');
			sortable('div.list', 'disable');
			element.textContent = 'Enable Sorting';
			this.sortingEnabled = false;
		}
		else
		{
			if (!this.sortingInit)
			{
				this.sortingInit = true;
				sortable('div.column', {
					items: 'div.box',
					acceptFrom: 'div.column'
				}).forEach(it =>
				{
					it.addEventListener('sortstart', this.sortStart.bind(this));
					it.addEventListener('sortstop', this.sortStop.bind(this, 'box'));
				});
				sortable('div.list', { items: 'div.row' }).forEach(it =>
				{
					it.addEventListener('sortstart', this.sortStart.bind(this));
					it.addEventListener('sortstop', this.sortStop.bind(this, 'list'));
				});
			}
			else
			{
				sortable('div.column', 'enable');
				sortable('div.list', 'enable');
			}
			element.textContent = 'Disable Sorting';
			this.sortingEnabled = true;
		}
	}

	sortStart(event)
	{
		document.querySelectorAll('div.box textarea, div.box input').forEach(it => it.disabled = true);
	}

	sortStop(type, event)
	{
		document.querySelectorAll('div.box textarea, div.box input').forEach(it => it.disabled = false);
		if (type == 'box')
		{
			let data = [];
			sortable('div.column', 'serialize').forEach(container =>
			{
				if (container.container.node.id != 'holderColumn')
				{
					let c = [];
					container.items.forEach(box => c.push(box.node.id));
					data.push(c);
				}
			});
			this.data.settings.boxPositions = data;
			this.export();
		}
	}

	download()
	{
		let dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.data, null, 4));
		let element = document.getElementById('download');
		let name    = (this.data.character.characterName ? this.data.character.characterName : 'UNNAMED') + '-' + (new Date).toISOString();
		element.setAttribute('href', dataStr);
		element.setAttribute('download', name.replace(/[^a-zA-Z0-9-_]+/g, '_')+ '.json');
	}

	upload(event)
	{
		let fileReader = new FileReader();
		fileReader.onload = ev =>
		{
			try
			{
				this.setData(ev.target.result);
			}
			catch(ex)
			{
				console.log(ex);
			}
		};
		fileReader.readAsText(event.target.files[0]);
	}

	camelCase(string)
	{
		let parts = string.replace(/\(.*\)/, '').replace(/[^a-zA-Z0-9- ]+/g, '_').split(/ /);
		let key = parts[0][0].toLowerCase() + parts[0].substring(1);
		for(let ii = 1; ii < parts.length; ++ii)
		{
			key += parts[ii];
		}
		return key;
	}

	setData(data)
	{
		window.localStorage.setItem('data', data);
		location.reload();
	}

	setPositions()
	{
		let pos = this.data.settings.boxPositions;
		if (Array.isArray(pos) && pos.length > 0)
		{
			let columns = document.querySelectorAll('div.page > div.column');
			if (columns.length == pos.length)
			{
				document.querySelectorAll('div.box:not(.header)').forEach(it => this.holderColumn.appendChild(it));
				for (let ii = 0; ii < pos.length; ++ii)
				{
					for (let jj = 0; jj < pos[ii].length; ++jj)
					{
						let box = document.getElementById(pos[ii][jj]);
						if (box)
						{
							columns[ii].appendChild(box);
						}
					}
				}
			}
		}
	}

	createField(label, id, isMultiLine)
	{
		id = typeof id == 'string' && id ? id : this.camelCase(label);

		let text = this.createNode({tagName: 'textarea', 'data-id': id, children: [ this.data.character[id] ]});
		let node = this.createNode({tagName: 'label', children: [ text, {tagName: 'span', children: [label]} ]});

		if (isMultiLine)
		{
			text.classList.add('multiline');
		}

		return node;
	}

	createListItem(value)
	{
		let row = this.createNode({tagName: 'div', class: 'row', children: [ {tagName: 'textarea', class: 'multiline', children: [ value ] } ]});

		return row;
	}

	createListField(label, id, withLabel, noExtraRows)
	{
		id = typeof id == 'string' && id ? id : this.camelCase(label);

		let node = this.createNode({tagName: 'div', class: 'list', 'data-id': id});

		Array.isArray(this.data.character[id]) && this.data.character[id].forEach(it => node.appendChild(this.createListItem(it)));

		if (noExtraRows === true)
		{
			if (!Array.isArray(this.data.character[id]) || this.data.character[id].length == 0)
			{
				node.appendChild(this.createListItem());
			}
		}
		else
		{
			for(let ii = 0; ii < this.extraRows; ++ii)
			{
				node.appendChild(this.createListItem());
			}
		}

		if (withLabel)
		{
			node.appendChild(this.createNode({tagName: 'label', children: [label]}));
		}

		return node;
	}

	createPage(className, headerFields)
	{
		let header  = this.createNode({ tagName: 'div', class: 'box header' });
		let column1 = this.createNode({ tagName: 'div', class: 'column' });
		let column2 = column1.cloneNode();
		let column3 = column1.cloneNode();
		let page    = this.createNode({ tagName: 'div', class: 'page ' + className, style: 'border-color: ' + this.borderColor + '; width: ' + this.pageWidth + 'mm; height: ' + this.pageHeight + 'mm;', children: [ header, column1, column2, column3 ] });
		document.body.appendChild(page);

		headerFields = Array.isArray(headerFields) ? headerFields : [];
		headerFields.unshift('Character Name');
		headerFields.forEach(it => header.appendChild(this.createField(it)));


		return { page: page, header: header, columns: [ column1, column2, column3 ] };
	}

	createBox(label, id, children)
	{
		id = this.camelCase('box ' + (typeof id == 'string' && id ? id : label));

		if (!Array.isArray(children))
		{
			children = [ children ];
		}
		if (typeof label == 'string' && label)
		{
			children.unshift( { tagName: 'div', class: 'label', children: [ label ]});
		}
		let node = this.createNode({ tagName: 'div', id: id, class: 'box', children: children });

		return node;
	}

	createSheet()
	{
		let spellsHeaderFields  = ['Spellcasting Class', 'Spellcasting Ability', 'Spell Save DC', 'Spell Attack Bonus'];
		let detailsHeaderFields = ['Age', 'Height' , 'Weight', 'Eyes', 'Skin', 'Hair'];

		let box = this.createBox('Spellcasting', null);
		spellsHeaderFields.forEach(it => box.appendChild(this.createField(it)));
		this.holderColumn.appendChild(box);

		box = this.createBox('Character Details', null);
		detailsHeaderFields.forEach(it => box.appendChild(this.createField(it)));
		this.holderColumn.appendChild(box);

		let mainPage    = this.createPage('main',    ['Race', 'Class', 'Level', 'Player Name', 'Alignment', 'Background', 'Experience Points']);
		let spellsPage  = this.createPage('spells',  spellsHeaderFields);
		let detailsPage = this.createPage('details', detailsHeaderFields);

		[1, 2, 3].forEach(it => this.createPage('extra' + it));

		/* AC, HIT DICE, HIT POINTS */
		box     = this.createBox(null, 'MultiHit');
		mainPage.columns[1].appendChild(box);
		['Armor Class', 'Initiative', 'Speed'].forEach(it => box.appendChild(this.createField(it)));
		['Maximum', 'Current', 'Temporary'].forEach(it => box.appendChild(this.createField('HP ' + it, this.camelCase('hitPoints' + it))));
		['Hit Dice Total', 'Hit Dice Used'].forEach(it => box.appendChild(this.createField(it)));

		box.appendChild(this.createNode({ tagName: 'div', class: 'deathSaves', children: [
			this.createField('Success', 'deathSavesSuccess'),
			this.createField('Failure', 'deathSavesFailure'),
			{ tagName: 'label', children: [ { tagName: 'span', children: ['Death Saves'] }] }
		]}));
		//['Exhausted', 'DC'].forEach(it => box.appendChild(this.createField(it)));

		/* BACKGROUND */
		box = this.createBox('Background');
		mainPage.columns[2].appendChild(box);
		['Details', 'Personality Traits', 'Ideals', 'Bonds', 'Flaws'].forEach(it => box.appendChild(this.createListField(it, this.camelCase('background ' + it), true, true)));

		/* ABILITY SCORES, SAVINGTHROWS, SKILLS */
		let current = this.createNode({ tagName: 'div' });
		box         = this.createBox(null, 'MultiAbilities', [ current, { tagName: 'div' } ]);
		mainPage.columns[0].appendChild(box);

		current.appendChild(this.createField('Inspiration'));
		current.appendChild(this.createField('Proficiency Bonus'));

		current.appendChild(this.createNode({ tagName: 'div' }));
		current.appendChild(this.createField('Passive Wisdom (Perception)'));

		current = current.children[2];
		['Strength', 'Dexterity', 'Constitution', 'Intelligence', 'Wisdom', 'Charisma'].forEach(it =>
		{
			let id = this.camelCase('abilityScore ' + it);
			let value = isNaN(this.data.character[id]) ? ' -': Math.floor((this.data.character[id] - 10) / 2);
			if (value > 0)
			{
				value = '+' + value;
			}
			current.appendChild(this.createNode(
				{ tagName: 'label', children: [
					{ tagName: 'span', children: [ value ]},
					{ tagName: 'textarea', 'data-id': id, children: [ this.data.character[id] ]},
					{ tagName: 'span', children: [it]}
				]}
			));
		});

		current = box.children[1];
		current.appendChild(this.createNode({ tagName: 'div' }));
		current = current.lastChild;
		['Strength', 'Dexterity', 'Constitution', 'Intelligence', 'Wisdom', 'Charisma'].forEach(it => current.appendChild(this.createCheck('savingThrow', it)));
		current.appendChild(this.createNode({tagName: 'label', children: [ {tagName: 'span', children: [ 'Saving Throws' ]} ]} ));

		current = box.children[1];
		current.appendChild(this.createNode({ tagName: 'div' }));
		current = current.lastChild;
		[
			'Acrobatics (Dexterity)',
			'Animal Handling (Wisdom)',
			'Arcana (Intelligence)',
			'Athletics (Strength)',
			'Deception (Charisma)',
			'History (Intelligence)',
			'Insight (Wisdom)',
			'Intimidation (Charisma)',
			'Investigation (Intelligence)',
			'Medicine (Wisdom)',
			'Nature (Intelligence)',
			'Perception (Wisdom)',
			'Performance (Charisma)',
			'Persuasion (Charisma)',
			'Religion (Intelligence)',
			'Sleight of Hand (Dexterity)',
			'Stealth (Dexterity)',
			'Survival (Wisdom)'
		].forEach(it => current.appendChild(this.createCheck('skill', it)));
		current.appendChild(this.createNode({tagName: 'label', children: [ {tagName: 'span', children: [ 'Skills' ]} ]} ));

		current = box.children[1];

		/* ATTACKS */
		current = this.createNode({ tagName: 'div', class: 'list', 'data-id': 'attacks' });
		box = this.createBox('Attacks & Spellcasting', 'Attacks', current);
		mainPage.columns[1].appendChild(box);

		Array.isArray(this.data.character.attacks) && this.data.character.attacks.forEach(it => current.appendChild(this.createAttackLine(it)));

		for (var ii = 0; ii < this.extraRows; ++ii)
		{
			current.appendChild(this.createAttackLine());
		}

		/* PROFICIENCIES */
		box = this.createBox('Proficincies', null, this.createListField('Proficiencies', null, false));
		mainPage.columns[0].appendChild(box);

		/* COINAGE */
		box = this.createBox('Coinage');
		mainPage.columns[1].appendChild(box);
		['Copper', 'Silver', 'Electrum', 'Gold', 'Platinum'].forEach(it =>
		{
			box.appendChild(this.createField(it, this.camelCase('coinage ' + it)));
		});

		/* EQUIPMENT */
		let name = 'Equipment';
		box = this.createBox(name, null, this.createListField(name, null, false));
		mainPage.columns[1].appendChild(box);

		/* FEATURES & TRAITS */
		name = 'Features';
		box  = this.createBox('Features & Traits', name, this.createListField(name, null, false));
		mainPage.columns[2].appendChild(box);

		/* Additional Stuff */
		['Notes 1', 'Notes 2', 'Notes 3', 'Rules 1', 'Rules 2', 'Rules 3'].forEach(name =>
		{
			box = this.createBox(name, null, this.createListField(name, null, false));
			this.holderColumn.appendChild(box);
		});

		/* SPELLS */
		spellsPage.page.setAttribute('data-id', 'spells');
		for (let ii = 0; ii < 10; ++ii)
		{
			let column = ii < 3 ? spellsPage.columns[0] : ii < 6 ? spellsPage.columns[1] : spellsPage.columns[2];
			column.appendChild(this.createSpellContainer(ii));
		}

		['Character Appearance', 'Character Backstory'].forEach(name =>
		{
			box = this.createBox(name, null, this.createListField(name, null, false));
			detailsPage.columns[0].appendChild(box);
		});
		['Allies & Organisations', 'Additional Features & Traits'].forEach(name =>
		{
			let id = this.camelCase(name.split('&')[0].trim());
			box    = this.createBox(name, null, this.createListField(name, id, false));
			detailsPage.columns[1].appendChild(box);
		});
		['Treasure'].forEach(name =>
		{
			box = this.createBox(name, null, this.createListField(name, null, false));
			detailsPage.columns[2].appendChild(box);
		});


		document.body.addEventListener('change', this.export.bind(this));
		document.body.addEventListener('keydown', this.export.bind(this));

		document.querySelectorAll('textarea.multiline').forEach(it => this.setTextareaHeight(it));

		this.export();
	};

	createCheck(prefix, name)
	{
		let ability      = name.match(/\((.+)\)/);
		ability          = (Array.isArray(ability) && ability.length > 1 ? ability[1] : name);
		name             = name.replace(/ \(.+\)/, '');
		let id           = this.camelCase(name);
		let noteId       = this.camelCase(prefix + ' Note ' + name);
		let proficient   = this.createNode({ tagName: 'input', type: 'checkbox', class: 'cb', id: 'cb-' + ++this.idCounter, 'data-id': prefix + 'Proficiencies', value: name });
		let isProficient = Array.isArray(this.data.character[prefix + 'Proficiencies']) && this.data.character[prefix + 'Proficiencies'].indexOf(name) >= 0;
		let bonus        = isNaN(this.data.character.proficiencyBonus) ? 0 : this.data.character.proficiencyBonus;
		let value        = isNaN(this.data.character['abilityScore' + ability]) ? ' -': Math.floor((this.data.character['abilityScore' + ability] - 10) / 2) + (isProficient ? (1*bonus) : 0);

		if (value > 0)
		{
			value = '+' + value;
		}

		let row = this.createNode({ tagName: 'div', class: 'row',  children: [
			proficient,
			{ tagName: 'label', for: 'cb-'  + this.idCounter, class: 'cb' },
			{ tagName: 'label', for: 'cb-'  + this.idCounter, class: 'cb print' },
			{ tagName: 'label', children: [
				{ tagName: 'span', children: [ value] },
				{ tagName: 'textarea', 'data-id': noteId, children: [this.data.character[noteId]] },
				{ tagName: 'span', children: [name + (ability != name ? ' (' + ability.substr(0, 3) + ')' : '')]}
			]},
		]});
		if (isProficient)
		{
			proficient.checked = true;
		}
		return row;
	}

	createAttackLine(data)
	{
		let row = this.createNode({ tagName: 'div', class: 'row',  children: [
			{ tagName: 'textarea', class: 'multiline name', children: [ data ? data.name: '' ] },
			{ tagName: 'textarea', class: 'bonus multiline', title: 'Attack Bonus', children: [ data ? data.bonus: '' ] },
			{ tagName: 'textarea', class: 'damage multiline', title: 'Damage/Type', children: [ data ? data.damage: '' ] }
		]});

		return row;
	}

	createSpellContainer(number)
	{
		let cSpells = typeof this.data.character.spells == 'object' ? this.data.character.spells[number] : null;
		let list    = this.createNode({ tagName: 'div', class: 'list' });
		let spells  = this.createNode({ tagName: 'div', id: 'boxSpells'+number, class: 'box spells', 'data-id': number, children: [
			{ tagName: 'div', class: 'head', children: [
				{ tagName: 'div', class: 'label', children: [ number == 0 ? 'Cantrips' : 'Spells ' + number ]},
				{ tagName: 'textarea', children: [ cSpells ? cSpells.slots : '' ] },
				{ tagName: 'textarea', children: [ cSpells ? cSpells.usedSlots : '' ] },
			] },
			list
		] });

		if (cSpells)
		{
			Array.isArray(cSpells.list) && cSpells.list.forEach(it => list.appendChild(this.createSpellLine(it)));
		}

		for (var ii = 0; ii < this.extraRows; ++ii)
		{
			list.appendChild(this.createSpellLine());
		}

		return spells;
	}

	createSpellLine(data)
	{
		let prepared = this.createNode({ tagName: 'input', type: 'checkbox', class: 'cb', id: 'cb-' + ++this.idCounter });
		let content  = this.createNode({ tagName: 'textarea', class: 'multiline', children: [ data ? data.content : '' ] });
		let row      = this.createNode({ tagName: 'div', class: 'row',  children: [
			prepared,
			{ tagName: 'label', for: 'cb-'  + this.idCounter, class: 'cb' },
			{ tagName: 'label', for: 'cb-'  + this.idCounter, class: 'cb print' },
			content
		]});
		if (data && data.prepared)
		{
			prepared.checked = true;
		}

		return row;
	}

	export(event)
	{
		this.data.character.spells = {};

		document.querySelectorAll('textarea[data-id]').forEach(it =>
		{
			let id = it.getAttribute('data-id');
			this.data.character[id] = it.value.trim();
		});

		document.querySelectorAll('[data-id]:not(textarea)').forEach(it =>
		{
			let id = it.getAttribute('data-id');
			if (id == 'attacks')
			{
				this.data.character.attacks = [];
				it.querySelectorAll('div.row').forEach(div =>
				{
					let name = div.querySelector('textarea.name').value.trim();
					if (name)
					{
						let attack = {
							name: name,
							bonus: div.querySelector('textarea.bonus').value.trim(),
							damage: div.querySelector('textarea.damage').value.trim()
						};
						this.data.character.attacks.push(attack);
					}
				});
			}
			else if (!isNaN(id))
			{
				this.data.character.spells['' + id] = {  list: []};
				if (id > 0)
				{
					this.data.character.spells[id].slots = it.querySelector('div.head textarea:first-of-type').value.trim();
					this.data.character.spells[id].usedSlots = it.querySelector('div.head textarea:last-child').value.trim();
				}

				it.querySelectorAll('div.row').forEach(div =>
				{
					let content = div.querySelector('textarea').value.trim();
					if (content)
					{
						let spell = { content: content };
						if (id > 0)
						{
							spell.prepared = div.querySelector('input').checked ? true : false;
						}
						this.data.character.spells[id].list.push(spell);
					}
				});
			}
			else if (it.classList.contains('list'))
			{
				this.data.character[id] = [];
				it.querySelectorAll('div.row').forEach(div =>
				{
					let value = div.querySelector('textarea').value.trim();
					if (value) {
						this.data.character[id].push(value);
					}
				});
			}
		});

		if (event)
		{
			let id = event.target.getAttribute('data-id');
			let isMultiLine = event.target.classList.contains('multiline');
			if (event.target.tagName == 'INPUT' && id && event.target.value)
			{
				if (!Array.isArray(this.data.character[id]))
				{
					this.data.character[id] = [];
				}
				let index = this.data.character[id].indexOf(event.target.value);
				if (event.target.checked && index == -1)
				{
					this.data.character[id].push(event.target.value);
				}
				else if (!event.target.checked && index > -1)
				{
					this.data.character[id].splice(index, 1);
				}
			}
			else if (event.target.tagName == 'TEXTAREA')
			{
				if (event.keyCode == 13 && (!isMultiLine || event.ctrlKey))
				{
					let inputs = document.querySelectorAll('div.page textarea');
					for (let ii = 0; ii < inputs.length; ++ii)
					{
						if (inputs[ii] == event.target)
						{
							inputs[++ii >= inputs.length ? 0 : ii].focus();
							break;
						}
					}
				}

				if (!isMultiLine && event.type == 'change' && id)
				{
					let value = event.target.value;
					this.data.character[id] = value.trim();
					document.querySelectorAll('textarea[data-id="' + id + '"]').forEach(it => { it.value = value; });
				}
				else if (isMultiLine)
				{
					this.setTextareaHeight(event.target);

					if (event.keyCode == 9 && !event.shiftKey && !event.ctrlKey)
					{
						event.preventDefault();
						event.stopPropagation();

						let start = event.target.selectionStart;
						let end   = event.target.selectionEnd;
						event.target.value          = event.target.value.substring(0, start) + "\t" + event.target.value.substring(end);
						event.target.selectionStart = start+1;
						event.target.selectionEnd   = start+1;
					}
				}
			}
		}

		Object.keys(this.data.character).forEach(it => {
			let element = document.querySelector('[data-id="' + it + '"]');
			if (!element)
			{
				delete(this.data.character[it]);
			}
		});

		document.getElementById('export').textContent = JSON.stringify(this.data, null, 4);

		window.localStorage.setItem('data', JSON.stringify(this.data));

	}

	setTextareaHeight(element)
	{
		if (element.parentElement.classList.contains('row'))
		{
			let maxHeight = 0;
			let elements  = element.parentElement.querySelectorAll('textarea.multiline');
			elements.forEach(it =>
			{
				it.style.overflow = 'auto';
				it.style.height   = '1px';
				maxHeight         = maxHeight < it.scrollHeight ? it.scrollHeight : maxHeight;
			});
			elements.forEach(it =>
			{
				it.style.height = (maxHeight + 1) + 'px';
				it.style.overflow = 'hidden';
			});
		}
		else
		{
			element.style.overflow = 'auto';
			element.style.height = '1px';
			element.style.height = (element.scrollHeight + 1) + 'px';
			element.style.overflow = 'hidden';
		}
	}

	createNode(data)
	{
		let node = null;
		if (data == null)
		{
		}
		else if (data instanceof HTMLElement)
		{
			node = data;
		}
		else if (data.tagName)
		{
			node = document.createElement(data.tagName);
			Object.entries(data).forEach(([key, value]) => { key != 'tagName' && key != 'children' && node.setAttribute(key, value); });
			Array.isArray(data.children) && data.children.forEach(it => { it != null && node.appendChild(this.createNode(it)); });
		}
		else if (typeof data != 'object')
		{
			node = document.createTextNode(data);
		}

		return node;
	};
};
